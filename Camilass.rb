load 'modulos/io.rb'
load 'modulos/objeto.rb'
load 'modulos/init.rb'
def cargar()
	modulos=[]
	
	File.open('modulos/modulos').each do |i|
		tmp=i.gsub("\n",'').split(":")
		begin
			load 'modulos/'+tmp[0]
		rescue LoadError
			print "\n\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;31m[Err][---]\e[m "+"Fallo carga de modulo#{tmp[1]}"+"\n"
		else
			print "\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;33m[Inf][---]\e[m "+"Carga Exitorsa de #{tmp[1]}"+"\n"
			modulos.push(tmp[1])
		end
	end
	return modulos
end
mod=cargar()
init(argumentos(),mod)