module Metadata
	def self.init(data)
		Io.printf("Metadata",'m',1)
		if data[:help]
			puts  "modulo: Metadata by s0cr4t35"
			puts "Nota: este modulo necesita de la apliacion exiftool ;)"
			puts "Metadata se encarga de extraer los metadatos de los arhivos de cualquier tipo (preferendte archivos office) (usuario, software,OS), para luego organizarlos"
			puts "Metadata:<parametros>"
			puts "              -full"
			puts "Este parametro especifica que se extraeran todos los metadatos de los arhivos que se ecnotraron en Indexer:archivos y luego se descargaron con Download:full, \nsi este no se especifica se descagara el archivo de la url y se procedera a extraer la informacion"
			puts "Nota : se utiliza luego de Download:full, para descargar los arhivos y luego extraer la informacion ;) "
			
		end
		if data[:full] and !data[:help]
			c=1
			$server.archivos.each do |nombre,valor|
				if valor[:download]
					Io.printf("archivo: #{nombre} #{((c*100.0)/$server.archivos.size).round(2)}% Completado..\n",'i',1)
					self.metadata(nombre)
				end
				c+=1
			end
		else
			Downloader.init()
			$server.archivos.each do |a,k|
				if k[:download]
					Io.printf("archivo: #{nombre} #{((c*100.0)/$server.archivos.size).round(2)}% Completado..\n",'i',1)
					self.metadata(a)
				end
			end
			
		end
	end
	def self.metadata(archivo)
		
		a=`exiftool download/#{archivo}`
		a.split("\n").each do |i|
			i=i.split(':')
			
			campo=i[0].strip
			valor=i[1..-1].join().strip
			case campo
			when 'Author','Last Saved By','Current User'
				$server.into_metadata('users',valor)
			when 'Directory'
				 $server.into_metadata('directorio',valor)
			when 'Primary Platform'
				$server.into_metadata('os',valor)
			when  'Creator Tool', 'Generator',  'Producer','Creator'
				$server.into_metadata('Software',valor)
			when 'Software'
				$server.into_metadata(campo,valor)
			when 'File Source', 'Make'
				$server.into_metadata(campo,valor)
			else
				Io.printf("#{i[0]}: #{i[1..-1]}\n",'i',3)
			end
		  
		end
	end
  
end
