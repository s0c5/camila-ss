module Io
	def self.printf(txt="",tipo='i',v=1,o='')
		@m=@m?@m:'Pot'
		
		if $server.vista>=v
			case tipo
			when 'i'
			print o+"\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;33m[Inf]["+@m+"]\e[m "+"+"*(v-1)+txt
			when 'e'
			print o+"\n\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;31m[Err]["+@m+"]\e[m "+"+"*(v-1)+txt+"\n"
			when 's'
			print o+"\n\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;31m[Str]["+@m+"]\e[m "+"+"*(v-1)+txt+"\n"
			when 'p'
			print txt+"\e[32m [POSIBLE]  \e[m\n"    
			when 'true'
			print txt+"\e[32m [OK]  \e[m\n"    
			when 'false'
			print txt+"\e[31m [FAIL]  \e[m\n"      
			when 'o'
			print o+"\n"+"\e[31m-"*40+"\e[m\n"+ txt+"\n"+"\e[31m-"*40+"\e[m\n"
			when 'm'
			print o+"\n\n"
			print o+"\n\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;32m[MOD]\e[m \e[1;41m"+txt.upcase+"\e[m\n"
			$server.modulo=txt
			@m=txt[0..2]
			end
		end
	  
	end
	def self.getf(txt,v=1)
		if $server.vista>=v
			print "\n\e[1m[#{Time.now.strftime("%H:%M:%S")}]\e[m\e[1;31m[Str]["+@m+"]\e[m "+"+"*(v-1)+txt
			a= STDIN.gets.chomp
			return a
		end
	end
	
	
end