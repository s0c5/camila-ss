module Save
	def self.init(data)
		
		if data[:path]
			if data[:path][-1]!='/'
				path=data[:path]+'/'+$server.uri.host
			else
				path=data[:path]+$server.uri.host
			end
		else
			path='cases/'+$server.uri.host
		end
		Io.printf('Guardando en '+path,'i',1)
		begin
			File.open(path+'.obj', 'w+') do |file|
				Marshal.dump($server, file)
			end
			File.open(path+'.cnf', 'w+') do |file|
				file.puts $server.modulo
				file.puts data
			end
		rescue
			Io.printf('','false',1)
			
		else
			Io.printf('','true',1)
		end
		
	end
  
  
end