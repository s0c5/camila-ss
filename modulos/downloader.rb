require 'open-uri'
module Downloader
	def self.init(data)
		Io.printf('Downloader','m',1)
		  if data[:help]
			 puts "\nModulo: Downloader By s0cr4t35"
			 puts "Nota: se recomienda utilizarlo luego de indexer:archivos"
			 puts "este modulo descarga el archivo especificado en la  url o "
			 puts "todos los archivos encontrados luego de Indexer:archivos\n"
			 puts "Downloader:<parametros>"
			 puts "full descarga todos los archivos encontrados en Indexer:archivos"
		  end
		  if !data[:full] and !data[:help]
			  nombre=$server.url.split('/')[-1]
			  self.download($server.url,nombre)
		  else
			self.archivos
		  end
	end
	def self.download(link,nombre)
	  begin
			open(link) {|f|  File.open('download/'+nombre,"w+") {|file|   file.puts f.read} }
		rescue URI::InvalidURIError
			Io.printf( " Url Invalida  #{nombre} Url: #{link}",'e',1)
		rescue OpenURI::HTTPError
			  Io.printf( " Http:Error  #{nombre} Url: #{link}",'e',1)
		rescue SocketError
		 else
			$server.archivos[nombre][:download]=true
			 Io.printf( "",'true',1)
	  end
	end
	def self.archivos
		key=$server.archivos.keys
		  c=0
		  while c<key.size
			  nombre=key[c]
			  link=$server.archivos[nombre]
			  Io.printf( "Download: #{nombre} #{((c*100.0)/$server.archivos.keys.size).round(2)}% Completado..",'i',1)
			begin 
				self.download(link[:url],nombre)
			end
		     
			c+=1
		end
	end
  
end
