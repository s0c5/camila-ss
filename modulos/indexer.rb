require 'net/http'
module Indexer
	def self.init(data)
		Io.printf("Indexer",'m',1)
		if data[:help]
			puts "\nModulo: Indexer By s0cr4t35"
			 puts "Este modulo escanea la url especificada en busca de achivos o parlabras"
			 puts "luego las guarda en el Objeto $server"
			 puts "indexer:<parametros>"
			 puts "         archivos: \n Nota : necesita el parametro --archivos, especifica las extenciones  de documentos a buscar:"
			 puts "office: busca las extenciones .doc .xls .ppt .pdf"
			 puts "imagenes: busca las extenciones .jpg .png .gif .bmp"
			 puts "<otros>: aqui usted especifica que otra extencion quiere buscar:"
			puts "Ejemplo: ruby potato.rb -u www.google.com -t 'Indexer:archivos' --archivos '.dbo:office'\n Nota: esto buscaria todos los archivos  office y con la extencion .dbo\n"
			puts "Ejemplo: ruby potato.rb -u www.google.com -t 'Indexer:archivos' --archivos 'imagenes:office:.xls:.torrent'\n Nota: esto buscaria todos los archivos  de imagenes, office y extenciones .xls y .torrent" 
			puts "NOTA: todas las rutas de los archivos y nombres de ellos se guardan en el hash $server.archivos" 
			puts "        busqueda: \n Nota: necesita del paramtro --busqueda, especifica las palabras a buscar cada una de ellas separadas por |:" 
			puts "Ejemplo: ruby potato.rb -u www.google.com -t 'Indexer:busqueda' --busqueda 'admin|Index of' \n esto buscaria estas palabras dentro de las paginas encontradas el resultado lo guarda en el hash $server.palarbas " 
			puts "NOTA: la busqueda tiene sensibilidad a las mayusculas y minusculas"
			puts " ---------------------------------------------------------------------------------------------------"
			puts " Ejemplos:  ruby potato.rb -u www.google.com -t 'Indexer:busqueda:archivos' --busqueda 'admin|Index of' --archivos 'office:.dbo'"
			
		end
		if data['archivos']
			tmp=data['archivos'].split(':')
			data['ext']='|('
			c=0
			ag='|'
			tmp.each do |i|
				if c+1==tmp.size
					ag=''
				end
				case i
				when 'office'
					data["ext"]+="\"\\S+\.doc\"|\"\\S+\.ppt\"|\"\\S+\.xls\"|\"\\S+\.pdf\""+ag
				when 'imagenes'
					data["ext"]+="\"\\S+\.jpg\"|\"\\S+\.png\"|\"\\S+\.gif\"|\"\\S+\.bmp\""+ag
				else 
					data["ext"]+="\"\\S+#{i.gsub('.','\.')}\""+ag
				end
				c+=1
			end
			data["ext"]+=')'

		end

		@pila=[]
		@pila.push($server.url)
		if !data[:help]
			self.indexer(data)
		end
	end
	def self.pila
		@pila
	end
	
	
	def self.indexer(data)
		re=/(")(http:\/\/#{$server.host.gsub('.','\.')}){0,1}([\w\+ \_\/]+)(\.php|\.aspx|\.html|\.shtml|\.htm|\.py|\.cgi|\/){1}(\?\S+=\S+&*)*(")|(")(http:\/\/#{$server.host.gsub('.','\.')}){0,1}([\w\+ \_\/]+)(\?\S+=\S+&*)+(")|(")(http:\/\/#{$server.host.gsub('.','\.')}){1}([\w\+ \_\/]+)(\?\S+=\S+&*)*(")#{data['ext']}/
		rf=/(\<form[\S =\r\n\t]+<\/form>)/
		c=0
		archivos=0
		pal=0
		while c<@pila.size
			
			Io.printf("Completado: #{((c*100.0)/@pila.size).round(2)}% Descargando: \""+@pila[c]+"\" ",'i',1)
			begin
				rta=$server.req(@pila[c])[0]
 			rescue Errno::ECONNRESET
 				Io.printf("Coneccion Perdida!",'e',1)
			rescue SocketError
				Io.printf("No encontrado",'e',1)
			else
				Io.printf("",'true',1)
			end
			Io.printf("PILA => Paginas: #{@pila.size} Actual: #{c}      Paginas=> total  #{$server.paginas.keys.size}  \n",'i',2)
			dir=self.dir(URI(@pila[c]).path)
			cookies= "";
			if rta['Set-Cookie']
				rta.get_fields('Set-Cookie').each do |cookie|
					 if cookie!=''
						$server.cookie=(cookie.split("; ")[0]+"; ")
					 end
					
				end
			end
			$server.cookies=$server.cookie
			Io.printf(rta.body,'o',6)
			#verifica el codigo si 200, empieza el proceso de indexacion
			if rta.code=='200'
				if data['busqueda']
					Io.printf("Palabras: #{pal} \n",'i',1)
					m=/#{data['busqueda'].gsub('.','\.')}/.match(rta.body)
					if  m
						if $server.into_palabras(m[0],@pila[c])
							Io.printf("Palabra Encontrada:  #{m[0]} \n",'i',3)
							pal+=1
						end
					end
				end 
				if data['archivos']
					Io.printf("Archivos: #{$server.archivos.keys.size} \n",'i',1)
				end
		    # aqui empieza la busqueda de la regular exprecion
				rta.body.scan(re) do |i|
					pag=i.join().gsub(/amp;|#038;/,'')
					if data['archivos']
						if pag=~/#{data['ext'][1..-1]}/
							if self.intog(pag,dir,'archivo',@pila[c])
								Io.printf("Archivo Encontrado: #{pag}\n",'i',3)
								archivos+=1
							end
							pag=''
						end
					end
					Io.printf("Pagina Encontrada: #{pag}\n",'i',4)
					self.intog(pag,dir,'pila',@pila[c])
				end
				if data['post']
					rta.body.scan(rf) do |form|
						  self.form(form.join(),@pila[c])
					end
				end
			else
		 #verifica el codigo de respuesta difernte a 200 si es 3XX lo redirije  
				case rta.code.to_i
				when (300..340)
					Io.printf( "Redireccion: \e[33;4m#{rta['location']}\e[m\n",'i',1)
					if rta['location'][0..4]=='http:' or rta['location'][0..5]=='https:'
						
						self.into(rta['location'].gsub('amp;',''))
						rta['location']=''
					end
					if rta['location'][0]=='/'  
						rta['location']='http://'+$server.host+rta['location']
						self.into(rta['location'].gsub('amp;',''))
					end
					if  rta['location'][0]=~/\w/ and dir.size>0 and rta['location'][0..4]!='http:'
						rta['location']='http://'+$server.host+dir+rta['location']
						self.into(rta['location'].gsub('amp;',''))
					end
					if  rta['location'][0]=~/\w/ and dir.size<=0 and rta['location'][0..4]!='http:'
						  rta['location']='http://'+$server.host+'/'+rta['location']
						self.into(rta['location'].gsub('amp;',''))
					end
					
				else
				Io.printf("Rta code: #{rta.code}  ",'e',1)
				end
			end
			c+=1
		end 
	end
	def self.form(form,pagina)
	      
		form.scan(/(<input[\w \S\$]+\/>)/) do |name|
			if name.join().match('<input')
				puts name
			end
		end
		
	 end
	def self.dira(dir,pag)
		if dir!='/'
			tmpp=pag.split('/')
			tmpd=dir.split('/')
			c=0 
			f=true
			d=0
			pagina=''
			while c<tmpp.size
				if tmpp[c]=='..' and f
					d+=1
				else
					f=false
					pagina+='/'+tmpp[c]
				end
				c+=1
			end
			n=(tmpd.size-1)-d
			dir=''
			tmpd.each do |i|
				if i!=''
					if n>0
						dir+='/'+i
						n-=1 
					else
						break
					end
				end
			end
		else
			pagina=pag.gsub('/..','')
		end
		
		return dir+pagina
	end

	def self.dirs(dir,pag)
		if dir!='/' and dir
			pag=pag.gsub(dir,'/')
		end
		return pag
	end
	def self.intog(pag,dir,action,ref)
		if $server.ssl
			ag='https://'
		else
			ag='http://'
		end
		if pag[0]=~/"|'/ and pag[-1]=~/"|'/
				pag[0]=''
				pag[-1]=''
		  end
		if self.dir(pag)==dir
			  dir='/'
		  end
		  if pag[0..4]=='http:' or pag[0..5]=='https:'
			if self.action(pag,action,ref)
				return true
			else 
				return false
			end
			pag=''
				
		  end
		  pag=self.dirs(dir,pag)
		  if pag[0..2]=='/..'
				  pag=self.dira(dir,pag)
		  end
		if pag[0]=='/'   
			if dir.size>0
				 pag[0]=''
			end
			pag=ag+$server.host+dir+pag
			if self.action(pag,action,ref)
				  return true
			else 
				  return false
			end
			pag=''
		 end
		 if pag[0]=~/\w/
				  if dir.size>0

					pag=ag+$server.host+dir+pag
				  else
				    pag=ag+$server.host+'/'+pag
				  end
				  if self.action(pag,action,ref)
					 return true
				  else 
					 return false
				  end
		end
	  
	end
	def self.action(pag,tipo,ref='')
		case tipo
		when 'pila'
			if self.into(pag)
				Io.printf("\e[32mPila OK:\e[m #{pag}\n",'i',5)
				return true
			else
				Io.printf("\e[31mPila FAIL:\e[m #{pag}\n",'i',5)
			end
		when 'archivo'
			if $server.into_archivo(pag,ref)
				 Io.printf("\e[32mArchivo OK:\e[m #{pag}\n",'i',5)
				  return true
			else
				Io.printf("\e[31mArchivo FAIL:\e[m #{pag}\n",'i',5)
			end
		end
	end
	def self.dir(path)
		  path=path.gsub(/(\?\S*)/,'')
		  m=path.match(/(([\w\-\.\_\+])+\/)+/)
		if m
			return '/'+m[0]
		else
			return '/'
		end
	end
	def self.into(pag)
		if !@pila.index(pag) 
			begin
				 $server.into_pagina(URI(pag))
				  @pila.push(pag)     
			rescue URI::InvalidURIError
				  Io.printf(" UrlError: #{pag}",'e',5)
				  return false
			else
				Io.printf("\e[32mUrlOK:\e[m #{pag}\n",'i',5)
				 return true
			end
		else
			return false
		end
	end
end
