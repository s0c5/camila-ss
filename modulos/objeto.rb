require 'timeout'
class Server
	def initialize(uri,data)
		@modulol=''
		@cve=[]
		@users=[]
		@cms={:com=>[],:cms=>data['cms']?data['cms']:false,:users=>{}}
		@sql={}
		@host=uri.host
		@port=uri.port
		@metadata={}
		@uri=uri
		@agent=data[:agent]
		@cookie=data[:cookie]
		@palabras={}
		@paginas={}
		@cookies=[]
		@cookies.push(@cookie)
		@paginas[uri.path]={:GET=>[uri.query] , :POST=>[data[:post]]}
		 
		@url=data[:url]
		if data[:url][0..5]=='https:'
			@ssl=true
		else
			@ssl=false
		end
		 @archivos={}
		@vista=data[:vista]?(data[:vista].to_i):1
		 
	end
	def modulo
		@modulo
	end
	def modulo=(x)
		@modulo=x
	end
	def cve
		@cve
	end
	def users
		@users
	end
	def into_users(x)
		if !@users.index(x)
			@users.push(x)
		end
	end
	def req(url,query='',head={})
		tmp=""
		
		i=Time.now.strftime("%s").to_i
		if url[0..$server.host.size-1]==$server.host 
			
			if $server.ssl
				urln='https://'
			else
				urln='http://'
			end
			url=urln+url
		end
		
		if url[0..4]!='http:' and url[0..5]!='https:'
			
			if $server.ssl
				urln='https://'
			else
				urln='http://'
			end
			urln+=$server.host+url
		else
			urln=url
			
		end
		
		uri=URI.parse(urln)
		
		if query!='' and !uri.query
			uri.query=query
		end
		data={'User-Agent' => $server.agent, 'Cookie'=>$server.cookie, 'Referer'=>$server.host}
		data=data.merge(head)
		req = Net::HTTP::Get.new(uri.request_uri, data)
		
		req.each do |k,v|
			tmp+= k+":"+v+"\n"
		end
		
		Io.printf(tmp,'o',6)
		if urln[0..4]=='http:'
			begin
				http = Net::HTTP.new(uri.host, uri.port)
				res = http.request(req) 
				
		  	rescue SocketError
					Io.getf("Error verifique la coneccion, precione la cualquier tecla para continuar",1)
			rescue Timeout::Error
				Io.printf("Timeout #{urln}  ",'e',1)
				Io.getf("verifique La coneccion y presiones (c) para continuar: ",1)
				 res = Net::HTTP.start(uri.host, uri.port) {|http|http.request(req) } 
			end
		else
			uri=URI.parse(urln)
			uri.query=URI::encode(query)
			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			request = Net::HTTP::Get.new(uri.request_uri,data)
			begin
			res = http.request(request) 
			rescue Errno::ECONNREFUSED
				  Io.getf("verifique La coneccion y presiones (c) para continuar: ",1)
				 res = http.request(request) 
				
			rescue SocketError
				Io.getf("verifique La coneccion y presiones (c) para continuar: ",1)
				res = http.request(request) 
			rescue Timeout::Error
				 Io.printf("Timeout #{urln}",'e',1)
				 Io.getf("verifique La coneccion y presiones (c) para continuar: ",1)
				 res = Net::HTTP.start(uri.host, uri.port) {|http|http.request(req) } 
			end
		end
		if res
			Io.printf(res.body,'o',6)
		end
		f=Time.now.strftime("%s").to_i
		return res, f-i
	end
	def sql
		@sql
	end
	def cms
		@cms
	end
	def cms=(x)
		@cms=x
	end
	def into_sql(pag,prm,pvul,payload)
		if !@sql.keys.index(pag)
			  @sql[pag]=[[prm,pvul,payload]]
		else
			  @sql[pag].push([prm,pvul,payload])
		end
	end
	def ssl
		@ssl
	end
	def vista
		@vista
	end
	def cookie=(x)
		if !into_cook(x)
		@cookie+= x
		else
		  
		  y=x.split('=')[0]
		  if x[-1]==' '
			  x[-1]=''
		  end
		  
		 @cookie=@cookie.gsub(/#{y}=\w+;/,x)
		end
		
	end
	def cookies
		@cookies
	end
	def nomcook(x)
		nom=''
		x.split(';').each do |i|
			nom+=i.split('=')[0]
		end
		return nom
	end
	def into_cook(x,t='c')
	  
	  case t
	  when 's'
	    x=nomcook(x)
	  @cookies.each do |i|
			
			  
			 if nomcook(i)==x
			   return true
			 end
		end
	  else
	    x=x.split('=')[0]
		   @cookie.split(';').each do |a|
				if a[0]==' '
					a[0]=''
				end
				if a.split('=')[0]==x
					return true
				end
			 end
			 
	  end
		return false
	end
	def cookies=(x)
	  if !into_cook(x,'s')
		@cookies.push(x)
	  end
	end
	def url
		@url
	end
	def host
		@host
	end
	
	def port
		@port
	end
	def uri
		@uri
	end
	def paginas
		@paginas
	end
	def cookie
		@cookie
	end
	def into_pagina(uri,data={:post=>''})
	  if !@paginas.keys.index(uri.path)
		  
		 @paginas[uri.path]={:GET=>[uri.query], :POST=>[data[:post]]}
		 return true
	  else
		
		
		if uri.query!=nil and @paginas[uri.path][:GET]!=nil and  @paginas[uri.path][:GET].size<20
			
			if !query_get(@paginas[uri.path],uri.query) 
				@paginas[uri.path][:GET].push(uri.query.gsub('amp;',""))
				
			end
			
		end
		if data[:post]!=nil and @paginas[uri.path][:POST].size<20
		  
			if !query_post(@paginas[uri.path],data[:post]) 
				@paginas[uri.path][:POST].push(data[:post])
			end
			
		end
	  end
	end
	def query_get(pagina,com)
		com=arg(com)
		pagina[:GET].each do |i|
			if arg(i)==com
			  return true
			end
		end
		return false
	end
	def query_post(pagina,com)
		if com==''
		  return true
		end
		com=arg(com)
		pagina[:POST].each do |i|
			if arg(i)==com
			  return false
			end
		end
		return true
	end
	def arg(arg)
		
		query=''
		
		if arg!=nil
		if arg[-1]=='&'
			arg[-1]=''
		end  
		if arg[0]=='&'
			arg[0]=''
		end
		  arg=arg.gsub("amp;","")
		  
		 begin 
			timeout(3) do 
				URI::decode_www_form(arg).each do |i|
				query+=i[0]+'|'
				end
			end
		 rescue ArgumentError
			Io.printf('Link:'+arg,'e',2)
			query=' '
		 rescue Timeout::Error
			Io.printf('Timeout arg:'+arg,'e',1)
		 end
		
		end
		
		return query
	end
	def agent
		@agent
	end
	def palabras
		@palabras
	end
	def into_palabras(palabra,pagina)
		if !@palabras.keys.index(palabra)
			@palabras[palabra]=[]
			return true
			@palabras[palabra].push(pagina)
		else
			 if !@palabras[palabra].index(pagina)
				@palabras[palabra].push(pagina)
			end
		end
	  
	end
	def into_archivo(pag,ref)
		nombre=pag.split('/')[-1]
		
		if !@archivos.keys.index(nombre)
			@archivos[nombre]={:url=>pag,:ref=>ref,:contador=>0,:download=>false}
			return true
		end
		
	end
	def metadata
		@metadata
	end
	def into_metadata(key,value)
		if !@metadata.keys.index(key)
			@metadata[key]=[]
			@metadata[key].push(value)
		else
			if !@metadata[key].index(value)
				@metadata[key].push(value)
			end
		end
	end
	def archivos
		@archivos
	end
end
