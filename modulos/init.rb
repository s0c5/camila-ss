require 'uri'
def argumentos()
	c=0
	arg={}
	while c<ARGV.size
		case ARGV[c]
		when '--load-full'
			arg[:loadf]=ARGV[c+1]
		when '--load'
			arg[:load]=ARGV[c+1]
		when '--o'
			arg[:path]=ARGV[c+1]
		when '-u' 
			arg[:url]=ARGV[c+1]
		when '-t'
			arg[:tipo]=ARGV[c+1]
		when '-v'
			arg[:vista]=ARGV[c+1]
		when '--post'
			arg[:post]=ARGV[c+1]
		when '--cookie'
			arg[:cookie]=ARGV[c+1]  
		when '--user-agent'
			case ARGV[c+1] 
			when 'google-linux'
				arg[:agent]="Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4"
			when 'firefox-linux'
				arg[:agent]="Mozilla/5.0 (X11; Linux i686; rv:14.0) Gecko/20100101 Firefox/14.0.1"
			else
				print Time.now.strftime("%H:%M:%S")+"[Inf] No se encontro navegador #{ARGV[c+1]} se establecio: Potato saw \n"
			end
		when '--busqueda'
			arg['busqueda']=ARGV[c+1] 
		when '--archivos'
			arg['archivos']=ARGV[c+1] 
		when '--archivo'
			arg['archivo']=ARGV[c+1] 
		when '--help','-h'
			logo()
			puts "\nPotato SAW (security aplication web), es una herramienta hecha  para testear la seguridad de apliaciones web de manera rapida\n y sencilla. Disfrutala\n\n"
			puts "ruby potato.rb <parametros>"
			puts "<parametros> <valor>: "
			puts "-u URL"
			puts "-t <modulos>"
			puts "-v regula las salida de informacion de 1 a 6 dependiendo de su importancia" 
			puts "--user-agent especifica el navegador: diponibles google-linux y firefox-linux"
			puts "--cookie estable las cookies "
			puts "--proxy establece proxy"
			puts "<modulos>: especifica el orden de los modulos a llamar con sus parametros de la siguiente manera:"
			puts "<Nombremodulo1>:<parametromodulo11>..|<Nombremodulo2>:<parametromodulo1>:<parametromodulo2>.."
			puts "ejemplo: \n\n esto Busca las paginas y sus parametros en una web, y luego llama al modulo sql para que compruebe si hay posibles puntos de inyeccion SQL a las paginas encontradas\n\n"
			puts "ruby potato.rb -u www.test.com -t 'Indexer|Sql:full'"
			puts "ejemplo: \n\n si quieres mas informacion acerca de un modulo coloca el parametro help\n\n"
			puts "ruby potato.rb -u www.google.com -t 'Indexer:help'"
			puts "\n modulos en esta version 0.2 alfa\n"
			puts "1) Indexer\n2) Sql\n3) Metadata\n4) Downloader"
			puts "Agradecimientos:"
			puts "A mi novia, Gracias a la cual en las noches no podia dormir y a la cual Amo gracias Camila jimenez "
			
			exit
		when '--tipo'
			arg['tipo']=ARGV[c+1] 
		when '--cms'
			arg[:cms]=ARGV[c+1] 
		when /--\w+/
			arg[ARGV[c].gsub('--','')]=ARGV[c+1] 
		end
		c+=1
	end
	return arg
end
def logo()
	print "\n\n"+" "*20+"\e[31m+++++++++++++++++++++++++++++++++++++++++++++++\e[m\n"
	print " "*20+"\e[31m+\e[m---\e[1;33m Camila Security Scanner v 0.2 alfa\e[m-------\e[31m+\e[m\n"
	print " "*20+"\e[31m+\e[m--------\e[1;33mBy: S0cr4t35  @potatogdit\e[m------------\e[31m+\e[m\n"
	print " "*20+"\e[31m+++++++++++++++++++++++++++++++++++++++++++++++\e[m\n"
end
def init(arg,modulos)
	logo()
	if  arg[:load]
			File.open(arg[:load]+'.obj') do |f|
				$server = Marshal.load(f)
			end
			modulo(arg,modulos)
	else
		 if arg[:loadf]
			File.open(arg[:loadf]+'.obj') do |f|
				$server = Marshal.load(f)
			end
			c=0
			mod=''
			File.open(arg[:loadf]+'.cnf').each  do |i|
				case c
				when 0
					mod=i.gsub("\n",'')
				when 1
					arg=eval(i.gsub("\n",''))
				end
				c+=1
			 end
			 modulo(arg,modulos,mod)
		 else
			if !arg[:post]
				  arg[:post]=''
			  end
			  if !arg[:cookie]
				    arg[:cookie]=''
			    end
			 if !arg[:agent]
				  arg[:agent]='Camila SS'
			end
			arg[:url]=URI::encode(arg[:url])
			if arg[:url][0..4]=='http:' || arg[:url][0..5]=='https:'
				  $server=Server.new(URI(URI::encode(arg[:url])),arg)
			else
				arg[:url]='http://'+arg[:url]
				$server=Server.new(URI(URI::encode(arg[:url])),arg)
			  end
			  modulo(arg,modulos)
		 end
	end

  
end
def modulo(arg,modulos,moda='')
	tmp=arg[:tipo].split('|')
	if moda!=''
		c=0
		tmp.each do |i|
			if i.split(':')[0].downcase==moda.downcase
				break
			else
				c+=1
			end
		end
	  else
		  c=0
	  end

	while c<tmp.size
		i=tmp[c].split(':')
		  data={}
		begin 
			
			modulos.each do |mod|
					if i[0].downcase==mod.downcase
						eval mod+'.init(subpara(i,arg))'
					end
		
			end
		 rescue SocketError
			    Io.printf("Compruebe host",'e',1)
			    exit
		rescue Timeout::Error
			Io.printf("Time out",'e',1)
		 rescue Interrupt
			ped()
		end
		c+=1
	end  
end
def subpara(i,arg)
			data={}
 			c=1
			while c<i.size
				case i[c]
				when 'full'
				  data[:full]=true
				when 'help'
				  data[:help]=true
				else  
				  if arg[i[c]]
					data[i[c]]=arg[i[c]]
				else
					Io.printf( "Warning:  Parametro Faltante \e[4m#{i[c]}\e[m valor por defecto TRUE",'e',1)
					data[i[c]]=true
				end
				end
				
				c+=1
			end
			return data
end
def ped()
		r=Io.getf("Que quires hacer (s)altar (e)xit o (g)uardar y salir: ")
			case r
			when 'e'
				exit
			when  's'
			when 'g'
				Save.init(argumentos())
				exit
			else
			  ped()
			end
end
