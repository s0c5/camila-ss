module Cms
	def self.init(data)
		Io.printf("Cms",'m')
		if data[:help]
			puts '-----------'
		end
		if data
			data.each do |nombre,valor|
			case nombre
			when 'forence'
				self.forence(data)
			when 'wps'
				self.wpsusers(data)
			end
			end
		else
			
		end
	end   
	def self.forence(data)
			if cms(data)
				Io.printf("Cms  #{$server.cms[:cms]}\n",'i',1)
				case $server.cms[:cms].split(':')[0]
				when 'joomla'
						data.each do |k,v|
						case k
						when 'com'
							self.joomcom(data)
						end
					end
				when 'wordpress'
					data.each do |k,v|
						case k
						when 'users'
							self.wpsusers(data)
						when 'com'
							self.wpcom(data)
						end
					end
				end
			else
				Io.printf("Cms No Detectada :(",'i',1)
			end
	end
	def self.joomcom(data)
		Io.printf("Joom-Components: starting ...\n",'i',1,"\n")
		
		req=$server.req($server.url)[0]
		req.body.scan(/com\_(\w*)/).each do |a|
			com='com_'+a.join()
			if !$server.cms[:com].index(com)
				$server.cms[:com].push(com)
				Io.printf("Componente: #{com}",'i',1,"\n")
			end
		end
		if data['cve']
			$server.cms[:com].each do |a|
				d={'search'=>a}
				Cve.cve(d)
			end
		end
	end
	def self.cms(data)
		  req=$server.req($server.url)[0]
		  
		  case req.code.to_i
		  when (300..399)
			  req=$server.req(req['location'])[0]
		  end
		  if !$server.cms[:cms] and req.body
			cms=req.body.match(/<meta name="generator" content="(.+)" \/>/)
			if cms
				case cms[1]
				  when /Joomla/
				
				    
					 req=$server.req($server.url+'/language/en-GB/en-GB.xml')[0]
					 if req.code=='200'
						version=req.body.match(/<version>(.+)<\/version>/)[1]
					 end
					if version
						
						 $server.cms[:cms]='joomla:'+version
					end
				when /WordPress/
						
						cms=cms[1].split(' ')
						version=cms[1..-1].join(' ')
						 $server.cms[:cms]=cms[0].downcase+':'+version
						 
				end
			end
			if !$server.cms[:cms]
				self.metapag(data)
			end
			 

		  end
		if $server.cms[:cms]
			return true
		else
			return false
		end
	end
	#
	#------------------------------------------------------------------------------------------------------------------------------#
	#
	def self.metapag(data)
		 cmsd={}
		  File.open('modulos/cms/pagmeta').each do |i|
			  i=i.gsub("\n",'')
			  tmp=i.split('|')
			  req=$server.req($server.url+tmp[0])[0]
			  Io.printf("Probando \e[1;32m#{tmp[0]}\e[m cms: \e[1;31m#{tmp[2]}\e[m",'i',1)
			  
			   case tmp[1]
			   when 'bool'
				 case req.code.to_i
				  when 200,403,500,302
					Io.printf("",'true',1)
					if !cmsd.keys.index(tmp[2])
						  cmsd[tmp[2]]=Array.new
						  
						  cmsd[tmp[2]][0]=1
					else
							if cmsd[tmp[2]][0]==nil
								 cmsd[tmp[2]][0]=1
							else
								cmsd[tmp[2]][0]+=1
							end
					end
				 when 404
					  Io.printf("",'false',1)
				 when (300..399)
					  Io.printf("redireccion \e[33m#{req['location']}\e[m\n",'i',1,"\n")
				 end
			   when 'txt'
				  if req.code.to_i==200 
					  Io.printf("",'true',1)
					  Io.printf("busqueda:  \e[33m#{tmp[3]}\e[m\n",'i',1)
					  a=false
					  b=false
					  c=0
					  while c<tmp[3].size
						if a and tmp[3][c-1]!="\\"
							case tmp[3][c]
							when '('
								b=true
							end
						end
						  c+=1
						  a=true
					  end
					  r=req.body.match(/#{tmp[3]}/)
					  if r
						  if b and r[1]
							  
							if !cmsd.keys.index(tmp[2])
								cmsd[tmp[2]]=Array.new
								cmsd[tmp[2]][1]=Array.new
								cmsd[tmp[2]][1].push(r[1])
							else
								
								if cmsd[tmp[2]][1]==nil
									 cmsd[tmp[2]][1]=Array.new
									 cmsd[tmp[2]][1].push(r[1])
								else
									cmsd[tmp[2]][1].push(r[1])
								end
							end
						  else
								if cmsd[tmp[2]][0]==nil
									 cmsd[tmp[2]][0]=1
								else
									cmsd[tmp[2]][0]+=1
								end
						  end
					  end
				  else
					 Io.printf("",'false',1)
				  end
			   end
		  end
		  
		  if cmsd!={}
				cmsd=cmsd.sort_by {|key,value| value[0]<=>key}
				$server.cms[:cms]=cmsd[0][0]
				if cmsd[0][1][1]
					
					$server.cms[:cms]+=':'+cmsd[0][1][1]
				else
					
					$server.cms[:cms]+=':'+'***'
				end
				    Io.printf("Cms #{$server.cms[:cms].split(':')[0]} #{$server.cms[:cms].split(':')[1]}\n",'i',1)
				return true
		  else
				return false
		  end
	end
	#
	#===================================================================================================+#
	#
	def self.wpcom(data)
		Io.printf(" Wp-plugins: starting ...\n",'i',1,"\n")
		c=1
		File.open('modulos/cms/plugins.txt').each do |i|
			if $server.uri.path[-1]=='/'
				url=$server.host+$server.uri.path+'wp-content/plugins/'+i
				
				
			else
				
				url=$server.host+$server.uri.path+'/wp-content/plugins/'+i
				
			end
			req=$server.req(url,'',{"range" => "bytes=0-700"})[0]
			if req
			  if c%15==0
				Io.printf("Plugins: #{ $server.cms[:com].size} Ultimo: #{$server.cms[:com].size>=0?$server.cms[:com][-1]:''} #{((c*100)/2156.0).round(2)}% completado..   #{req.code} \n",'i',1)
			  end
			 Io.printf("Plugin: #{i.split('/')[0]} #{((c*100)/2156.0).round(2)}% completado..  #{req.code}",'i',2)
			 
			end
			
			if req and !req.body.match('Error 404 - Not Found')
				case req.code.to_i
				when 200,500,206
					Io.printf("",'true',2)
					$server.cms[:com].push(i.split('/')[0])
				else
					Io.printf("",'false',2)
				end
			else
				Io.printf("",'false',2)
			end
			c+=1
		end
		if data['cve']
			da={}
			$server.cms[:com].each do |i|
				da['search']=i
				Cve.cve(da)
			end
		end
	end
	
	def self.wpsusers(data)
		Io.printf(" Wp-users: starting ...\n",'i',1,"\n")
		if data['usersn']=~/\d+/ 
		  n=data['usersn'].to_i
		else
			Io.printf("No se especifico parametro usersn valor por defecto 25 \n",'i',1)
			n=25
		end
			for i in (0..n)
				req=$server.req($server.url+'/?author='+i.to_s)[0]
				Io.printf("Probando: "+$server.url+'/?author='+i.to_s,'i',1)
				  case req.code.to_i
				  when 200
					
					user=req.body.match(/title="View all posts by \w+">([\w\-\_.]+)<\/a>|title="([\w\-\_.]+)" rel="me">(\w.+)<\/a>|\/author\/([\w\-\_]+)\/feed\//)
					
					if user
						user=user.captures.to_a
						user=user.map!{|i| 
						               if i==nil 
									' '
						              else 
									i
						              end
						               }
						user=user.sort {|i,a| a<=>i}
						Io.printf(" ("+user[0]+')','true',1)
						$server.into_users(user[0])
					else
						Io.printf("",'false',1)
					end
				  when (300..399)
					  r=req['location'].match(/author\/(.+)/)
					  if r
						if r[1][-1]=='/'
							name=r[1].split('')[0..r[1].size-2].join()
						else
							name=r[1]
						end
						Io.printf(" ("+name+')','true',1)
						$server.into_users(name)
					  else
						Io.printf("",'false',1)
					  end
				else
					  Io.printf("",'false',1)
				end
			end
	end
	
end
